﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Grid;

namespace WindowsFormsApplication18
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private int _id ;
        private int _rowHanle ;
        private void Form1_Load(object sender, EventArgs e)
        {
            FillData();
            var phone = new RepositoryItemTextEdit();
            phone.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            phone.Mask.EditMask = @"+(000)000-00-0000";
            gridControl1.RepositoryItems.Add(phone);
            gridView1.Columns["Phone"].ColumnEdit = phone;
            var dateMasked = new RepositoryItemDateEdit();
            dateMasked.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            dateMasked.EditMask = @"yyyy-MMM-d";
            gridControl1.RepositoryItems.Add(dateMasked);
            gridView1.Columns["BirthDay"].ColumnEdit = dateMasked;

            var email = new RepositoryItemTextEdit();
            email.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            email.Mask.EditMask = @"(\w|[\.\-])+@(\w|[\-]+\.)*(\w|[\-]){2,63}\.[a-zA-Z]{2,4}";
            gridControl1.RepositoryItems.Add(email);
            gridView1.Columns["Email"].ColumnEdit = email;
            var lfName = new RepositoryItemTextEdit();
            lfName.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            lfName.Mask.EditMask = @"\w{0,50}";
            gridControl1.RepositoryItems.Add(lfName);
            gridView1.Columns["FirstName"].ColumnEdit = lfName;
            gridView1.Columns["LastName"].ColumnEdit = lfName;
        }
        
        private void FillData()
        {
            try
                {
                    var db = new work_person();
                    var gridDataList = db.People.ToList();
                    gridControl1.DataSource = gridDataList;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(Convert.ToString(ex));
                }
            
        }

        private int GetMaxPersonId()
        {
            try
            {
                using (var db = new work_person())
                {
                    return db.People.Max(a => a.ID);
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }

        private void InsertToPerson()
        {

            try
            {
                int id = GetMaxPersonId();
                id++;
                using (var db = new work_person())
                {
                    var person = new Person {ID = id, Phone = ""};
                    db.People.Add(person);
                    db.SaveChanges();
                }
                FillData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void DeleteFromPerson()
        {
            const MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            try
            {

                using (var db = new work_person())
                {
                    var personWork = db.Works.Where(a => a.PersonID == _id);
                    var peson = db.People.First(p => p.ID == _id);
                    DialogResult result = MessageBox.Show(@"delete  person?", @"delete", buttons);
                    if (result == DialogResult.Yes)
                    {
                        if (personWork.Any())
                        {
                            result = MessageBox.Show(@"delete all personal information?", @"delete", buttons);
                            if (result == DialogResult.Yes)
                            {
                                db.Works.RemoveRange(personWork);
                                db.People.Remove(peson);
                                db.SaveChanges();
                                gridView1.DeleteRow(_rowHanle);
                            }
                        }
                        else
                        {
                            db.People.Remove(peson);
                            db.SaveChanges();
                            gridView1.DeleteRow(_rowHanle);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            InsertToPerson();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            DeleteFromPerson();
        }

        private void gridView1_CellValueChanged(object sender,
            DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            try
            {
                using (var db = new work_person())
                {
                    var person = db.People.First(a => a.ID == _id);
                    if (e.Column.FieldName == "Phone")
                        person.Phone = e.Value.ToString();
                    if (e.Column.FieldName == "BirthDay")
                    {
                        if (e.Value.ToString() != "")
                            person.BirthDay = Convert.ToDateTime(e.Value);
                        else person.BirthDay = null;
                    }
                    if (e.Value.ToString().Length > 70)
                        MessageBox.Show(@"Max Lenght limited for column " + e.Column);
                    else
                    {
                        if (e.Column.FieldName == "LastName")
                            person.LastName = e.Value.ToString();
                        if (e.Column.FieldName == "FirstName")
                            person.FirstName = e.Value.ToString();
                        if (e.Column.FieldName == "Email")
                            person.Email = e.Value.ToString();
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void gridControl1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && _clickChek)
                InsertToPerson();
            _clickChek = true;
        }

        private void gridControl1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (_clickChek)
                {
                    toolStripMenuItem1.Visible = false;
                    toolStripMenuItem3.Visible = false;
                    toolStripSeparator1.Visible = false;
                    menu.Show(MousePosition.X, MousePosition.Y);
                }
                else
                {
                    toolStripMenuItem1.Visible = true;
                    toolStripMenuItem3.Visible = true;
                    toolStripSeparator1.Visible = true;
                    menu.Show(MousePosition.X, MousePosition.Y);
                }
            }
            _clickChek = true;
        }

        private bool _clickChek = true;

        private void toolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            CreateFormWork();
        }

        private void toolStripMenuItem2_Click_1(object sender, EventArgs e)
        {
            InsertToPerson();
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            DeleteFromPerson();
        }
   
        private void CreateFormWork()
        {
            try
            {
                var firstName = gridView1.GetFocusedRowCellValue("FirstName");
                var lastName = gridView1.GetFocusedRowCellValue("LastName");
                var form2 = new Form2(Convert.ToInt32(_id), Convert.ToString(firstName),
                    Convert.ToString(lastName));
                form2.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void gridView1_RowCellClick(object sender, RowCellClickEventArgs e)
        {
            _clickChek = false;
            if (e.Clicks == 2 && e.Button == MouseButtons.Left)
            {
                if (e.Column.FieldName == "ID")
                CreateFormWork();
                if(e.Column.FieldName=="Photo")
                    CreateFormPhoto();
            }
        }
       
        private void CreateFormPhoto()
        {
            try
            {
                var form3 = new Form3(Convert.ToInt32(_id))
                {
                    Text =
                        gridView1.GetFocusedRowCellValue("FirstName") + @" " +
                        gridView1.GetFocusedRowCellValue("LastName")
                };
                form3.Show();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            try
            {
                _id = (int) gridView1.GetRowCellValue(e.FocusedRowHandle, "ID");
               _rowHanle= e.PrevFocusedRowHandle;
            }
            catch (Exception)
            {}
        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            FillData();
        }

        private void gridView1_RowStyle(object sender, RowStyleEventArgs e)
        {
            if (e.RowHandle % 2 == 0)
                e.Appearance.BackColor = Color.LightGray;
        }
    }
}
