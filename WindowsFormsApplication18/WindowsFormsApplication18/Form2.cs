﻿using System;
using System.Globalization;
using System.Windows.Forms;
using System.Linq;

namespace WindowsFormsApplication18
{
    public partial class Form2 : Form
    {
        int _id;
       
        public Form2(int id, string firstName, string lastName)
        {
            InitializeComponent();
            _id = id;
            idToolStripTextBox1.Text = Convert.ToString(_id);
            toolStripLabel3.Text = firstName;toolStripLabel4.Text = lastName;
            idToolStripTextBox1.Text = id.ToString(CultureInfo.InvariantCulture);
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            FillData(_id);
        }
        private void fillByToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                _id = Convert.ToInt32(idToolStripTextBox1.Text);
                if (_id == 0)
                {
                    toolStripLabel3.Text = "";
                    toolStripLabel4.Text = "";
                    gridView1.Columns.ColumnByFieldName("PersonID").Visible = true;
                }
                else
                {
                    gridView1.Columns.ColumnByFieldName("PersonID").Visible = false;
                    using (var db = new work_person())
                    {
                        var person = db.People.First(a => a.ID == _id);
                        toolStripLabel3.Text = person.FirstName;
                        toolStripLabel4.Text = person.LastName;
                    }

                    FillData(_id);}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FillData(int id)
        {
            try
            {
                var db = new work_person();
                var gridDataList = db.Works.Where(a => a.PersonID == id).ToList();
                if (id == 0)
                    gridDataList = db.Works.ToList();
                gridControl1.DataSource = gridDataList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(Convert.ToString(ex));
            }
        }

        private  int GetMaxWorksId()
        {
            try
            {
                using (var db = new work_person())
                {
                    return db.Works.Max(a => a.ID);
                }
            }
            catch (Exception )
            {
                return 0;
            }
        }

        private void InsertToWork(int personId)
        {
            try
            {
                int worksId = GetMaxWorksId();
                worksId++;
                using (var db = new work_person())
                {
                    var work = new Work { ID = worksId, PersonID = personId,StartDate = DateTime.Now};
                    db.Works.Add(work);
                    db.SaveChanges();
                }
                FillData(_id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DeleteFromWork(int id)
        {
            const MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result = MessageBox.Show(@"delete personal information?", @"delete", buttons);
            if (result == DialogResult.Yes)
            {
                try
                {
                    using (var db = new work_person())
                    {
                        var person = db.Works.First(a => a.ID == id);
                        db.Works.Remove(person);
                        db.SaveChanges();
                    }
                    FillData(_id);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            DeleteFromWork((int)gridView1.GetFocusedRowCellValue("ID"));
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            
             InsertToWork(_id);
            
        }
        private void gridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            var id = (int)gridView1.GetRowCellValue(e.RowHandle, "ID");
            try
            {
                using (var db = new work_person())
                {
                    var person = db.Works.First(a => a.ID == id);
                    if (e.Value.ToString().Length > 50)
                        MessageBox.Show(@"Max Lenght limited for column " + e.Column);
                    else
                    {
                        if (e.Column.ToString() == "Compani Name")
                            person.CompaniName = e.Value.ToString();
                        if (e.Column.ToString() == "Job Position")
                            person.JobPosition = e.Value.ToString();
                    }
                    if (e.Column.ToString() == "Start Date")
                    {
                        if (e.Value.ToString() != "")
                        person.StartDate = Convert.ToDateTime(e.Value);
                        else person.EndDate = null;
                    }
                    if (e.Column.ToString() == "End Date")
                    {
                        if (e.Value.ToString() !="" )
                            person.EndDate = Convert.ToDateTime(e.Value);
                        else person.EndDate = null;
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void gridView1_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            if (e.RowHandle % 2 == 0)
                e.Appearance.BackColor = System.Drawing.Color.LightGray;
        }

        bool _clickChek = true;

        private void gridControl1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && _clickChek)
                InsertToWork(_id);
            _clickChek = true;
        }
        private void gridView1_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            _clickChek = false;
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DeleteFromWork((int)gridView1.GetFocusedRowCellValue("ID"));
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            InsertToWork(_id);
        }

        private void gridControl1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (_clickChek)
                {
                    toolStripMenuItem1.Visible = false;
                    contextMenuStrip1.Show(MousePosition.X, MousePosition.Y);
                }
                else
                {
                    toolStripMenuItem1.Visible = true;
                    contextMenuStrip1.Show(MousePosition.X, MousePosition.Y);  
                }
            }
            _clickChek = true;
        }        
    }
}
